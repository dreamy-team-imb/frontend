const functions = require("firebase-functions");
const admin = require('firebase-admin');
const sgMail = require('@sendgrid/mail');

/* Initializations */
admin.initializeApp();
sgMail.setApiKey(functions.config().sendgrid.key);


exports.sendEmail = functions.firestore.document('certificates/{docid}').onCreate((snap, context) => {
    //Object with data of created certificate
    const certificate = snap.data();

    if (certificate.send == true) {

        //Variables from certificate that will go to mail
        let serial = certificate.serial
        let ccMails = certificate.ccMails

        let nonDuplicated = ccMails.filter((v, i, a) => a.indexOf(v) === i);
        
        //let instructor = certificate.instructor

        //Student reference
        let studentRef = certificate.student

        //Get student data by reference
        studentRef.get().then(student => {

            //Set variables that will go to mail
            let fullname = student.data().name
            let email = student.data().email
            let name = fullname.split(" ")[0]

            let cleanMails = nonDuplicated.filter(e => e !== email)
            //Send mail
            sgMail.send({
                to: email,
                from: "trajnime@imb.al",
                cc: cleanMails,
                subject: "Certifikata IMB",
                templateId: "d-41442a2a16f74f4faefddfc84cc8748d",
                dynamic_template_data: {
                    fullname: fullname,
                    name: name,
                    link: `https://certificate.imb.al/view.html?serial=${serial}`,
                    subject: "Certifikata IMB"
                }
            }).then(m => {
                console.log(`Email sent to ${email}.`);
                console.log(JSON.stringify(m));
                return;
            }).catch(e => {
                console.log(`Failed to send email to ${email}.`);
                console.error(JSON.stringify(e))
                return;
            })
        })

    }

});


//Assign admin custom claim
exports.assignClaims = functions.https.onRequest((req, res) => {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Methods', 'POST, OPTIONS');

    if (req.method == 'POST') {

        let uid = req.body.uid
        let claimAction = req.body.claim

        if (claimAction.toString() == "delete") {
            return admin.auth().setCustomUserClaims(uid, { admin: false }).then(() => {
                res.json({ result: `Deleted admin custom claim of user with ID: ${uid}` })
            }).catch((error) => {
                res.json({ result: `Couldn't set custom claim to user with ID: ${uid}` })
            });
        }
        else if (claimAction.toString() == "add") {
            return admin.auth().setCustomUserClaims(uid, { admin: true }).then(() => {
                res.json({ result: `Added admin custom claim to user with ID: ${uid}` })
            }).catch((error) => {
                res.json({ result: `Couldn't set custom claim to user with ID: ${uid}` })
            });
        }
        else {
            res.json({ result: "This claim action is not available.", status: 400 })
        }


    }
    else if (req.method == 'OPTIONS') {
        res.status(200).end()
    }
    else {
        res.status(403).end()
    }

});




//Create User Cloud Function
exports.createUser = functions.https.onRequest((req, res) => {

    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Methods', 'POST, OPTIONS');

    if (req.method == 'POST') {
        let name = req.body.name
        let role = req.body.role
        let email = req.body.email
        let password = req.body.password


        //Creates user
        admin
            .auth()
            .createUser({
                email: email,
                emailVerified: false,
                password: password,
                displayName: name,
                disabled: false,
            })
            .then((userRecord) => {
                // See the UserRecord reference doc for the contents of userRecord.
                console.log('Successfully created new user:', userRecord.uid);

                // Push the new message into Firestore using the Firebase Admin SDK.
                admin.firestore().collection('users').add({ email: email, username: name, uid: userRecord.uid, role: role, status: true });

                res.json({ result: `User with doc ID: ${userRecord.uid} added.`, uid: userRecord.uid });
            })
            .catch((error) => {
                console.log('Error creating new user:', error);
            });
    }
    else if (req.method == 'OPTIONS') {
        res.status(200).end()
    }
    else {
        res.status(403).end()
    }
});


exports.updateUser = functions.firestore.document('users/{userId}').onUpdate((change, context) => {

    //Get object with user updated data
    const user = change.after.data();

    //Get fileds that are going to be used
    const status = user.status;
    const uid = user.uid


    /* Check the updated status and 
            enable || disable
        the user depenfing on it */
    if (status == true) {

        //Get user by uid and enable him
        admin
            .auth()
            .updateUser(uid, {
                disabled: false,
            })
            .then((userRecord) => {
                // See the UserRecord reference doc for the contents of userRecord.
                console.log('Successfully updated user', userRecord.toJSON());
            })
            .catch((error) => {
                console.log('Error updating user:', error);
            });
    }
    else if (status == false) {

        //Get user by uid and disable him
        admin
            .auth()
            .updateUser(uid, {
                disabled: true,
            })
            .then((userRecord) => {
                // See the UserRecord reference doc for the contents of userRecord.
                console.log('Successfully updated user', userRecord.toJSON());
            })
            .catch((error) => {
                console.log('Error updating user:', error);
            });
    }

});




//Only used to get user data for adding features
/*
exports.findUsername = functions.https.onRequest((req, res) => {

    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Methods', 'GET, OPTIONS');

    if (req.method == 'GET') {

        let email = req.body.email

        admin
            .auth()
            .getUserByEmail(email)
            .then((userRecord) => {
                // See the UserRecord reference doc for the contents of userRecord.
                console.log(`Successfully fetched user data: ${userRecord.toJSON()}`);
                res.json({user : userRecord, udata: userRecord})
            })
            .catch((error) => {
                console.log('Error fetching user data:', error);
            });
    }
    else if (req.method == 'OPTIONS') {
        res.status(200).end()
    }
    else {
        res.status(403).end()
    }
});
*/


