//Selectors
let passwordModalBtn = document.querySelector(".passwordModalBtn");
let passwordModalBody = document.querySelector(".passwordModal-body");
let changePasswordBtn = document.querySelector(".changePasswordBtn");
let closeModal = document.querySelector(".closeModal");
let paginationBtns = document.querySelector(".paginationBtns");
let currentUserAdmin;

//Check for user login
firebaseApp.auth().onAuthStateChanged(function (user) {
  showLoader();
  if (user) {
    $(".logIn").remove();
    $(".certificateUser").remove();
    document.querySelector(".username").innerHTML =
      user.displayName + '<i class="pl-1 fa fa-user"></i>';

    document.querySelector(".passwordModal-body").innerHTML =
      "Do të marrësh një email tek: " +
      user.email +
      " për të ndryshuar fjalëkalimin?";

    passwordModalBtn.addEventListener("click", () => {
      $("#passwordModal").modal("show");
    });

    changePasswordBtn.addEventListener("click", () => {
      firebaseApp
        .auth()
        .sendPasswordResetEmail(user.email)
        .then((success) => {
          console.log("Password reset email sent.");
          $("#passwordModal").modal("hide");
        })
        .catch((error) => {
          // Error occurred. Inspect error.
          console.log(error);
        });
    });

    closeModal.addEventListener("click", () => {
      $("#passwordModal").modal("hide");
    });

    getCert()
      .then((data) => {
        console.log(data);
        renderTableData(data);
        $(document).ready(function () {
          var certTable = $("#cert-list").DataTable({
            language: {
              zeroRecords: " ",
              paginate: {
                next: '<i class="fa fa-angle-right"></i>', // or '→'
                previous: '<i class="fa fa-angle-left"></i>',
              },
            },
            lengthChange: false,
            pagingType: "simple",
            paging: false,
            searching: false,
            bInfo: false,
            order: [],

            //Can be enabled for instructor and serial, doesn't work for student
            columnDefs: [
              { orderable: false, targets: 2 },
              { orderable: false, targets: 3 },
              { orderable: false, targets: 4 },
              { orderable: false, targets: 5 },
              { orderable: false, targets: 0 },
            ],
          });
        });
      })
      .catch((err) => {
        console.log(err);
      });

    firebaseApp
      .auth()
      .currentUser.getIdTokenResult()
      .then((idTokenResult) => {
        // Confirm the user is an Admin.
        if (idTokenResult.claims.admin) {
          currentUserAdmin = true;
        } else {
          $(".adminDir").remove();
          console.log("Not admin");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  } else {
    $("#cert-list").hide();
    $(".signOutButton").remove();
    $(".certificateAdmin").remove();
    $(".userAccount").remove();
  }
});

//Selectors
let $certificateTable = $(".cert-table");
let tableContainer = document.querySelector("#table-wrapper");

let searchByName = document.querySelector(".searchByName");
let searchByID = document.querySelector(".searchByID");

let filterByName = document.querySelector("#filterByName");
let filterById = document.querySelector("#filterById");

let nameFilter = document.querySelectorAll(".nameFilter");
let idFilter = document.querySelectorAll(".idFilter");

let searchByNameBtn = document.querySelector(".searchByNameBtn");
let searchByIDBtn = document.querySelector(".searchByIDBtn");
let studentArr = [];

function renderTableData(data) {
  $certificateTable.html("");

  if (data.length) {
    for (let i = 0; i < data.length; i++) {
      const dt = new Date(data[i].date.toDate());
      const formattedDate = `${dt.getDate()}/${
        dt.getMonth() + 1
      }/${dt.getFullYear()}`;

      let certType = "";
      if (data[i].type === "user") {
        certType = "Përdorues Alpha";
      } else if (data[i].type === "instructor") {
        certType = "Instruktor Alpha";
      } else if (data[i].type.startsWith("data-")) {
        certType = data[i].type.replace("data-", "Data ");
      }

      const studentName = data[i].studentData ? data[i].studentData.name : "";
      const studentId = data[i].student ? data[i].student.id : "";

      const certificate = `
                <tr class="cert-row">
                    <td>${certType}</td>
                    <td data-order="${data[i].date.seconds}">${formattedDate}</td>
                    <td>${data[i].instructor}</td>
                    <td data-studentid="${studentId}">${studentName}</td>
                    <td>${data[i].serial}</td>
                    <td>
                        <button class="btn btn-white viewCert" id="${data[i].serial}" data-docid="${data[i].serial}">
                            <i class="fa fa-eye" style="pointer-events: none;"></i>
                        </button>
                    </td>
                </tr>
            `;

      $certificateTable.append(certificate);
    }
  } else {
    let noRecordMessage = "Nuk u gjet asnje certifikate me keto te dhena.";
    if (
      !currentUserAdmin &&
      (filterByName.value || filterById.value) &&
      data.length === 1
    ) {
      noRecordMessage = "";
    }
    $certificateTable.append(
      `<tr><td colspan="6">${noRecordMessage}</td></tr>`
    );
  }

  // Attach click event listeners to view buttons
  const viewCertButtons = document.querySelectorAll(".viewCert");
  viewCertButtons.forEach((button) => {
    button.addEventListener("click", (e) => {
      window.location = `/view.html?serial=${e.target.getAttribute(
        "data-docid"
      )}`;
    });
  });
}

// function renderTableData(data) {

//     $certificateTable.html('')
//     let styleProp = ""

//     if (data.length) {
//         let certType;
//         let certificate;

//         for (let i = 0; i < data.length; i++) {
//             let dt = new Date(data[i].date.toDate())
//             let formated = `${dt.getDate()}/${dt.getMonth() + 1}/${dt.getFullYear()}`

//             if (!currentUserAdmin) {
//                 if (data[i].unlisted == true) {
//                     if ((filterByName.value || filterById.value) && data.length == 1) {
//                         styleProp = "style='display: none !important'"
//                         $certificateTable.append('Nuk u gjet asnje certifikate me keto te dhena.')
//                     }
//                     else {
//                         styleProp = "style='display: none !important'"
//                     }
//                 }
//             }

//             if (data[i].type == "user") {
//                 certType = "Përdorues Alpha"
//             }
//             else if (data[i].type == "instructor") {
//                 certType = "Instruktor Alpha"
//             }
//             else if (data[i].type == "data-analyst") {
//                 certType = "Data Analyst"
//             }
//             else if (data[i].type == "data-flutter") {
//                 certType = "Data Flutter"
//             }
//             else if (data[i].type == "data-delta") {
//                 certType = "Data Delta"
//             }

//             certificate = `
//                 <tr class="cert-row" ${styleProp}>
//                 <td>${certType}</td>
//                 <td data-order=${data[i].date.seconds}>${formated}</td>
//                 <td>${data[i].instructor}</td>
//                 <td data-studentid="${data[i].student.id}">${data[i].studentData != null ? data[i].studentData.name: ""}</td>
//                 <td>${data[i].serial}</td>
//                 <td><button class="btn btn-white viewCert" id=${data[i].serial} data-docid=${data[i].serial}><i class="fa fa-eye" style="pointer-events: none;"></i></button></td></tr>'
//                 `

//             $certificateTable.append(certificate)
//             styleProp = ""

//         }

//         let viewCert = document.querySelectorAll('.viewCert')
//         viewCert.forEach(el => {
//             el.addEventListener("click", (e) => {
//                 window.location = `/view.html?serial=${e.target.getAttribute('data-docid')}`
//             })
//         })

//     } else {

//         $certificateTable.append('Nuk u gjet asnje certifikate me keto te dhena.')
//     }
// }

searchByName.addEventListener("click", () => {
  nameFilter.forEach((el) => {
    el.classList.remove("hide");
  });

  idFilter.forEach((el) => {
    el.classList.add("hide");
  });
});

searchByID.addEventListener("click", () => {
  idFilter.forEach((el) => {
    el.classList.remove("hide");
  });

  nameFilter.forEach((el) => {
    el.classList.add("hide");
  });
});

//Add event
searchByNameBtn.addEventListener("click", () => {
  let searchedName = filterByName.value;
  if (searchedName) {
    paginationBtns.classList.add("hide");
    $("#cert-list").DataTable().destroy();
    getCertByName({
      studentName: searchedName,
    })
      .then((data) => {
        hideLoader();
        $("#cert-list").show();
        renderTableData(data);
      })
      .catch((err) => {
        hideLoader();
        console.log(err);
      });
  } else {
    window.location.reload();
  }
});

searchByIDBtn.addEventListener("click", () => {
  let searchedId = filterById.value;
  paginationBtns.classList.add("hide");
  $("#cert-list").DataTable().destroy();
  if (searchedId.length > 0) {
    getOneCertBySerial(searchedId, true)
      .then((data) => {
        hideLoader();
        $("#cert-list").show();
        renderTableData(data);
      })
      .catch((err) => {
        hideLoader();
        console.log(err);
      });
  } else {
    window.location.reload();
  }
});

//Sign out
let signOutButton = document.querySelector(".signOutButton");
signOutButton.addEventListener("click", () => {
  signOut()
    .then((success) => {
      window.location.reload();
      window.location = "/";
    })
    .catch((error) => {
      console.log(error);
    });
});
