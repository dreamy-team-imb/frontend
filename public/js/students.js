//Selectors
let passwordModalBtn = document.querySelector('.passwordModalBtn')
let passwordModalBody = document.querySelector('.passwordModal-body')
let changePasswordBtn = document.querySelector('.changePasswordBtn')
let closeModal = document.querySelector('.closeModal')
let paginationBtns = document.querySelector('.paginationBtns')
let currentUserAdminStudents;
let currentUserStudents;
let studentForm = "#add-student-form-page"
let addStudentBtn = document.querySelector('.addStudentBtn')


let studentName = document.querySelector('.student-name')
let studentPhone = document.querySelector('.student-phone')
let studentEmail = document.querySelector('.student-email')

//Check for user login
firebaseApp.auth().onAuthStateChanged(function (user) {
    showLoader()
    if (user) {
        currentUserStudents = user
        $('.logIn').remove()
        $('.certificateUser').remove()
        document.querySelector('.username').innerHTML = user.displayName + '<i class="pl-1 fa fa-user"></i>'

        document.querySelector('.passwordModal-body').innerHTML = 'Do të marrësh një email tek: ' + user.email + ' për të ndryshuar fjalëkalimin?'

        passwordModalBtn.addEventListener("click", () => {
            $('#passwordModal').modal('show')
        })

        changePasswordBtn.addEventListener("click", () => {
            firebaseApp.auth().sendPasswordResetEmail(
                user.email)
                .then((success) => {
                    console.log("Password reset email sent.")
                    $('#passwordModal').modal('hide')
                })
                .catch((error) => {
                    // Error occurred. Inspect error.
                    console.log(error)
                });
        })

        closeModal.addEventListener("click", () => {
            $('#passwordModal').modal('hide')
        })


        firebaseApp.auth().currentUser.getIdTokenResult().then((idTokenResult) => {

            // Confirm the user is an Admin.
            if (idTokenResult.claims.admin) {

                currentUserAdminStudents = true
                getAllStudentsAdmin().then(data => {
                    renderTableData(data)
                    $(document).ready(function () {
                        var studentTable = $('#student-list').DataTable({
                            language: {
                                "zeroRecords": " ",
                                paginate: {
                                    next: '<i class="fa fa-angle-right"></i>', // or '→'
                                    previous: '<i class="fa fa-angle-left"></i>'
                                }
                            },
                            "lengthChange": false,
                            "pagingType": "simple",
                            paging: false,
                            "searching": false,
                            "bInfo": false,
                            "order": [],

                            //Can be enabled for instructor and serial, doesn't work for student
                            columnDefs: [
                                { orderable: false, targets: 0 },
                                { orderable: false, targets: 1 },
                                { orderable: false, targets: 2 },
                                { orderable: false, targets: 3 },
                                { orderable: false, targets: 4 }

                            ]
                        });

                    });
                }).catch(err => {
                    console.log(err)
                })


            } else {

                getMyStudents(user.uid).then(data => {
                    renderTableData(data)
                    $(document).ready(function () {
                        var studentTable = $('#student-list').DataTable({
                            language: {
                                "zeroRecords": " ",
                                paginate: {
                                    next: '<i class="fa fa-angle-right"></i>', // or '→'
                                    previous: '<i class="fa fa-angle-left"></i>'
                                }
                            },
                            "lengthChange": false,
                            "pagingType": "simple",
                            paging: false,
                            "searching": false,
                            "bInfo": false,
                            "order": [],

                            //Can be enabled for instructor and serial, doesn't work for student
                            columnDefs: [
                                { orderable: false, targets: 0 },
                                { orderable: false, targets: 1 },
                                { orderable: false, targets: 2 },
                                { orderable: false, targets: 3 },
                                { orderable: false, targets: 4 }

                            ]
                        });

                    });
                }).catch(err => {
                    console.log(err)
                })

            }
        }).catch((error) => {
            console.log(error);
        });







    } else {
        window.location = '/'
        $('#student-list').hide()
        $('.signOutButton').remove()
        $('.certificateAdmin').remove()
        $('.userAccount').remove()
    }
});



//Selectors
let $studentsTable = $(".student-table");
let tableContainer = document.querySelector('#table-wrapper')
let openStudentModal = document.querySelector('#open-students-modal');

let searchByName = document.querySelector('.searchByName')
let searchByEmail = document.querySelector('.searchByEmail')

let filterByName = document.querySelector('#filterByName')
let filterByEmail = document.querySelector('#filterByEmail')

let nameFilter = document.querySelectorAll('.nameFilter')
let emailFilter = document.querySelectorAll('.emailFilter')

let searchByNameBtn = document.querySelector('.searchByNameBtn')
let searchByEmailBtn = document.querySelector('.searchByEmailBtn')
let studentArr = []



function renderTableData(data) {
    $studentsTable.html('')
    let styleProp = ""

    if (data.length) {

        let student;

        for (let i = 0; i < data.length; i++) {

            // <td data-studentid="${data[i].student.id}">${data[i].studentData.name}</td>
            // <td><button class="btn btn-white viewCert" id=${data[i].serial} data-docid=${data[i].serial}><i class="fa fa-eye" style="pointer-events: none;"></i></button></td>
            student = `
                <tr class="cert-row" ${styleProp}>
                    <td>${data[i].code}</td>
                    <td>${data[i].email}</td>
                    <td>${data[i].name}</td>
                    <td>${data[i].phone}</td>
                    <td>
                        <button 
                            class="btn btn-white editStudent" 
                            data-name="${(data[i].name)}"
                            data-email="${data[i].email}"
                            data-id="${data[i].id}"
                            data-phone="${data[i].phone}"
                        >
                            <i class="fa fa-pencil" style="pointer-events: none;"></i>
                        </button>

                        <button 
                            class="btn btn-white viewStudentsCert" 
                            data-id="${data[i].id}"
                            data-name="${(data[i].name)}"
                        >
                            <i class="fa fa-file-o mt-2" style="pointer-events: none;"></i>
                        </button>

                    </td>'
                </tr>'
                `

            $studentsTable.append(student)
            styleProp = ""

        }

        let editStudentBtns = document.querySelectorAll('.editStudent')
        editStudentBtns.forEach(button => {
            button.addEventListener("click", (e) => {

                $('#student-name-edit').val(button.dataset.name)
                $('#student-phone-edit').val(button.dataset.phone)
                $('#student-email-edit').val(button.dataset.email)

                $('#editStudentBtn').attr('data-id', button.dataset.id)

                $('#student-modal-edit').modal('show')
            })
        })


        let viewStudentsCertBtns = document.querySelectorAll('.viewStudentsCert')
        viewStudentsCertBtns.forEach(button => {
            button.addEventListener("click", (e) => {

                $('#student-cert-title').text(`Certifikatat e ${button.dataset.name}`)

                getAllStudentsCerts(button.dataset.id).then(certs => {
                    console.log(certs)
                    if (certs?.length > 0) {

                        let certHtml = "";

                        certs.map(cert => {

                            certHtml += `
                                <div onclick="window.location='/view.html?serial=${cert.serial}'" pointer class="w-75 d-flex mb-2 py-3 mb-3 pointer shadow-sm border border-1 justify-content-center align-items-center text-primary rounded">
                                    <span class="fs-3">${cert.serial}</span>
                                </div>
                            `;

                        })


                        $('#student-cert-body').html(certHtml)
                    }
                    else {
                        $('#student-cert-body').html("<span class='fs-4'>Ky student nuk ka asnjë certifikatë.</span>")
                    }


                }).catch(err => {
                    console.log(err)
                })

                $('#student-modal-certs').modal('show')
            })
        })

    } else {

        $studentsTable.append('Ju nuk keni asnje student.')
    }
}

const closeStudentsCerts = document.querySelector('.close-student-certs')
closeStudentsCerts.addEventListener('click', () => {
    $('#student-modal-certs').modal('hide')
})


const editStudentBtn = document.getElementById('editStudentBtn')
//Edit a student
editStudentBtn.addEventListener("click", (e) => {
    e.preventDefault();

    if (validateForm('#student-page-edit')) {
        //Set up last code
        const user = {
            name: $('#student-name-edit').val(),
            phone: $('#student-phone-edit').val(),
            email: $('#student-email-edit').val()
        }

        const id = editStudentBtn.dataset.id
        if (id) {
            $('#student-modal').modal('hide')
            showLoader();
            editStudentById(id, user)
        }
    }

})


searchByName.addEventListener("click", () => {

    nameFilter.forEach(el => {
        el.classList.remove('hide')
    })

    emailFilter.forEach(el => {
        el.classList.add('hide')
    })

})

searchByEmail.addEventListener("click", () => {
    emailFilter.forEach(el => {
        el.classList.remove('hide')
    })

    nameFilter.forEach(el => {
        el.classList.add('hide')
    })
})


//Add event
searchByNameBtn.addEventListener("click", () => {
    let searchedName = filterByName.value
    if (searchedName) {
        paginationBtns.classList.add('hide')
        $('#student-list').DataTable().destroy()

        if (currentUserAdminStudents) {
            getStudentByNameAdmin(searchedName).then(data => {
                hideLoader()
                $('#student-list').show()
                renderTableData(data)
            }).catch(err => {
                hideLoader()
                console.log(err)
            })
        }
        else {
            getStudentByName(searchedName, currentUserStudents.uid).then(data => {
                hideLoader()
                $('#student-list').show()
                renderTableData(data)
            }).catch(err => {
                hideLoader()
                console.log(err)
            })
        }

    }
    else {
        window.location.reload()
    }
})

//Add a student addStudent(data)
addStudentBtn.addEventListener("click", (e) => {
    e.preventDefault();

    //Set up last code
    lastStudentCode.get().then(doc => {

        let newCode = doc.data().code

        if (validateForm(studentForm)) {
            $('#student-modal').modal('hide')
            showLoader();
            addStudentPage({
                name: studentName.value,
                phone: studentPhone.value,
                email: studentEmail.value,
                code: inc(doc.data().code),
                createdBy: currentUserStudents.uid
            })
        }
    })
})

openStudentModal.addEventListener('click', () => {
    $('#student-modal').modal('show')
})


searchByEmailBtn.addEventListener("click", () => {
    let searchedMail = filterByEmail.value
    paginationBtns.classList.add('hide')
    $('#student-list').DataTable().destroy()
    if (searchedMail.length > 0) {

        if (currentUserAdminStudents) {
            getStudentByEmailAdmin(searchedMail).then(data => {
                hideLoader()
                $('#student-list').show()
                renderTableData(data)

            }).catch(err => {
                hideLoader()
                console.log(err)
            })
        }
        else {
            getStudentByEmail(searchedMail, currentUserStudents.uid).then(data => {
                hideLoader()
                $('#student-list').show()
                renderTableData(data)

            }).catch(err => {
                hideLoader()
                console.log(err)
            })
        }

    }
    else {
        window.location.reload()
    }
})


//Sign out
let signOutButton = document.querySelector('.signOutButton')
signOutButton.addEventListener("click", () => {
    signOut().then((success) => {
        window.location.reload()
        window.location = "/"
    }).catch((error) => {
        console.log(error)
    })
});