//Selectors
if (window.location.hash == "#contact-us") {
    document.body.scrollIntoView(false);
}


let signOutButton = document.querySelector('.signOutButton')

let passwordModalBtn = document.querySelector('.passwordModalBtn')
let passwordModalBody = document.querySelector('.passwordModal-body')
let changePasswordBtn = document.querySelector('.changePasswordBtn')
let closeModal = document.querySelector('.closeModal')


firebaseApp.auth().onAuthStateChanged(function (user) {
    showLoader()
    if (user) {
        $('.logIn').remove()
        $('.certificateUser').remove()
        document.querySelector('.username').innerHTML = user.displayName + '<i class="pl-1 fa fa-user"></i>'

        document.querySelector('.passwordModal-body').innerHTML = 'Do të marrësh një email tek: ' + user.email + ' për të ndryshuar fjalëkalimin?'
        
        passwordModalBtn.addEventListener("click", () => {
            $('#passwordModal').modal('show')
        })

        changePasswordBtn.addEventListener("click", () => {
            firebaseApp.auth().sendPasswordResetEmail(
            user.email)
            .then((success) =>  {
                console.log("Password reset email sent.")
                $('#passwordModal').modal('hide')
            })
            .catch((error) => {
                // Error occurred. Inspect error.code.
                console.log(error)
            });
        })

        closeModal.addEventListener("click", () => {
            $('#passwordModal').modal('hide')
        })

        firebaseApp.auth().currentUser.getIdTokenResult()
            .then((idTokenResult) => {
                // Confirm the user is an Admin.
                if (idTokenResult.claims.admin) {


                } else {

                    $('.adminDir').remove()
                    console.log('Not admin')

                }
            })
            .catch((error) => {
                console.log(error);
            });

    } else {
        $('.signOutButton').remove()
        $('.certificateAdmin').remove()
        $('.userAccount').remove()
        $('.studentsNav').remove()
    }
});



//Log out user
signOutButton.addEventListener("click", () => {
    signOut().then((success) => {

        window.location.reload()
    }).catch((error) => {
        console.log(error)
    })
});