let passwordModalBtn = document.querySelector(".passwordModalBtn");
let passwordModalBody = document.querySelector(".passwordModal-body");
let changePasswordBtn = document.querySelector(".changePasswordBtn");
let closeModal = document.querySelector(".closeModal");
let currentuser;
let currentUserCert;

//Check for user login
firebaseApp.auth().onAuthStateChanged(function (user) {
  showLoader();
  if (user) {
    currentuser = user;
    currentUserCert = user;
    $(".logIn").remove();
    $(".certificateUser").remove();
    document.querySelector(".username").innerHTML =
      user.displayName + '<i class="pl-1 fa fa-user"></i>';

    document.querySelector(".passwordModal-body").innerHTML =
      "Do të marrësh një email tek: " +
      user.email +
      " për të ndryshuar fjalëkalimin?";

    passwordModalBtn.addEventListener("click", () => {
      $("#passwordModal").modal("show");
    });

    changePasswordBtn.addEventListener("click", () => {
      firebaseApp
        .auth()
        .sendPasswordResetEmail(user.email)
        .then((success) => {
          console.log("Password reset email sent.");
          $("#passwordModal").modal("hide");
        })
        .catch((error) => {
          // Error occurred. Inspect error.code.
          console.log(error);
        });
    });

    closeModal.addEventListener("click", () => {
      $("#passwordModal").modal("hide");
    });

    firebaseApp
      .auth()
      .currentUser.getIdTokenResult()
      .then((idTokenResult) => {
        // Confirm the user is an Admin.
        if (idTokenResult.claims.admin) {
        } else {
          $(".adminDir").remove();
          console.log("Not admin");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  } else {
    showLoader();
    window.location = "/";
    $(".signOutButton").remove();
    $(".certificateAdmin").remove();
    $(".userAccount").remove();
  }
});

// Selectors
let $selectStudent = $(".student-list");
let certType = document.querySelector(".cert-type");
let certDate = document.querySelector(".cert-date");
let certInstructor = document.querySelector(".cert-instructor");
let certStudent = document.querySelector(".cert-student");
let listedCert = document.querySelector("#listed");
let addCertBtn = document.querySelector(".addCertBtn");
let addStudentBtn = document.querySelector(".addStudentBtn");
let instructorMail = document.querySelector("#instructor-cc");

//Email button selectors
let sendTrue = document.querySelector(".sendEmailBtn");
let sendFalse = document.querySelector(".dontsendEmailBtn");

let studentName = document.querySelector(".student-name");
let studentPhone = document.querySelector(".student-phone");
let studentEmail = document.querySelector(".student-email");

let form = "#add-certificate";
let studentForm = "#add-student";

function renderStudents(data) {
  // $selectStudent.append(
  //   '<option class="open-modal" value="shto-student" >Marrës i ri</option>'
  // );
  if (data.length) {
    for (let i = 0; i < data.length; i++) {
      let studentOption = `
                <option value="${data[i].name}">${data[i].name}</option>
                `;
      $selectStudent.append(studentOption);
    }
  }
}

getAllStudents()
  .then((data) => {
    renderStudents(data);
  })
  .catch((err) => {
    console.log(err);
  });

// Events
//Add a certificate
addCertBtn.addEventListener("click", (e) => {
  e.preventDefault();
  if (validateForm(form)) {
    // showLoader();

    $("#emailModal").modal({ backdrop: "static", keyboard: false });
    $("#emailModal").modal("show");
  }
  localStorage.clear();
});

///////////////////////////////////////////////////////////
sendTrue.addEventListener("click", (e) => {
  e.preventDefault();
  showLoader();


  lastCertCode.get().then((doc) => {
    let newCertCode = doc.data().code;

    const selectedStudents = $("#student-select").val();
    const promises = selectedStudents.map(async (studentName) => {
      return getStudent({ studentName }).then(async (student) => {
        const cert_ref = await certCollection
          .where("studentId", "==", student.id)
          .where("type", "==", certType.value)
          .get();

        if (cert_ref.docs.length !== 0) {
          hideLoader();
          alert(`Kjo certifikate ekziston per studentin:${studentName}`);
          return;
        }

        // const number = selectedStudents.map(async (studentName) => {
        //   return getStudent({ studentName }).then(async (student) => {
        //     const getNumber = await studentCollection.where(
        //       "name",
        //       "== ",
        //       studentName.value
        //     )
        //     .where("phone","==",studentPhone.value);
        //     get();
        //     if (getNumber.length !== 0) {
        //       hideLoader();
        //       return getNumber;
        // const body = "u dergua";
        // sendSms(getNumber, body);
        //     }
            let lastCC = inc2(doc.data().code);
            let studentCode = student.code;
            let serial = studentCode + "-" + lastCC;


            return addCert({
              serial: serial,
              type: certType.value,
              date: new Date(certDate.value),
              instructor: certInstructor.value,
              student: db.doc("students/" + student.id),
              reference: studentCollection.doc(student.id),
              unlisted: listedCert.checked,
              send: true,
              studentId: student.id,
              ccMails: [currentuser.email, instructorMail.value],
            });
          });
        });

        Promise.all(promises)
          .then(() => {
            clearForm(form);
            $("#student-select").val(null).trigger("change");
            updateCertCode({ code: inc2(newCertCode) });
            hideLoader();
            instructorMail.innerHTML = "";
          })
          .catch((error) => {
            console.log(error);
            hideLoader();
          });
      });
    });

sendFalse.addEventListener("click", () => {
  showLoader();

  lastCertCode.get().then((doc) => {
    let newCertCode = doc.data().code;

    getStudent({ studentName: certStudent.value })
      .then((student) => {
        let lastCC = inc2(doc.data().code);
        let studentCode = student.code;
        let serial = studentCode + "-" + lastCC;

        addCert({
          serial: serial,
          type: certType.value,
          date: new Date(certDate.value),
          instructor: certInstructor.value,
          student: db.doc("students/" + student.id),
          reference: studentCollection.doc(student.id),
          send: false,
          unlisted: listedCert.checked,
          studentId: student.id,
        });

        clearForm(form);

        //Show validation on select
        $("#student-select").val(null).trigger("change");

        //Update last code in settings
        updateCertCode({
          code: inc2(newCertCode),
        });
        hideLoader();
      })
      .catch((error) => {
        console.log(error);
      });
  });
});

//Add a student addStudent(data)
addStudentBtn.addEventListener("click", (e) => {
  e.preventDefault();

  studentCollection
    .where("email", "==", studentEmail.value)
    .where("name", "==", studentName.value)
    .get()
    .then((querySnapshot) => {
      if (!querySnapshot.empty) {
        // A student with the same email already exists
        alert(`Studenti ${studentName.value} ka nje llogari egzistuese.`);
        return;
      }
      //Set up last code
      lastStudentCode.get().then((doc) => {
        console.log(doc.data().code);
        let newCode = doc.data().code;

        if (validateForm(studentForm)) {
          showLoader();
          addStudent({
            name: studentName.value,
            phone: studentPhone.value,
            email: studentEmail.value,
            code: inc(doc.data().code),
            createdBy: currentUserCert.uid,
          });

          // let newStudent = `
          //  <option value="${studentName.value}">${studentName.value}</option>
          //  `;

          // $selectStudent.append(newStudent);
          // $(".student-list").val(studentName.value)
          // $(".student-list").trigger("change");
          // $("#exampleModal").modal("hide");
          // hideLoader();
          // console.log(studentName.value);
          // console.log(newStudent);



let savedvalue = [];
function saveSelectedValues(values) {
  localStorage.setItem('selectedStudents', JSON.stringify(values));
}

// Function to retrieve the selected values from localStorage
function getSavedValues() {
  const savedValues = localStorage.getItem('selectedStudents');
  return savedValues ? JSON.parse(savedValues) : [];
}

// Create a new option element with the student name
let newStudent = `
           <option value="${studentName.value}">${studentName.value}</option>
           `;

// Append the new option to the existing student list
$(".student-list").append(newStudent);
console.log(localStorage);
// Retrieve the existing saved values
savedValues = getSavedValues();

// Add the newly selected value to the array
savedValues.push(studentName.value);

// Save the updated array to localStorage
saveSelectedValues(savedValues);

// Set the value of the dropdown to the newly added student
$(".student-list").val(savedValues);

// Trigger the change event to ensure any associated event handlers are executed
$(".student-list").trigger("change");

// Hide the modal and hide the loader
$("#exampleModal").modal("hide");
hideLoader();

// Log the saved values to the console for debugging
console.log('Saved values:', savedValues);
futPerdoruesit.addEventListener("click", (e) => {
  e.preventDefault();
  localStorage.clear();
  console.log('u klikua')
  console.log(savedValues);
  console.log(savedValues);
  console.log(localStorage);

  })

          updateCode({
            code: inc(newCode),
          });
        }
      });
    });
});
//Sign out
let signOutButton = document.querySelector(".signOutButton");
signOutButton.addEventListener("click", () => {
  signOut()
    .then((success) => {
      window.location.reload();
    })
    .catch((error) => {
      console.log(error);
    });
});
