const createUser = (name, role, email, password) => {
    return new Promise((resolve, reject)=> {
        $.ajax({
            url: '//us-central1-imb-digital-cert.cloudfunctions.net/createUser',
            method: 'POST',
            data: {name, role, email, password},
        })
        .done(res => resolve(res))
        .fail(err => reject(err))
        
    })
}


const makeAdmin = (uid, claim) => {
    console.log(uid)
    return new Promise((resolve, reject)=> {
        $.ajax({
            url: '//us-central1-imb-digital-cert.cloudfunctions.net/assignClaims',
            method: 'POST',
            data: {uid, claim},
        })
        .done(res => resolve(res))
        .fail(err => reject(err))
    })
}