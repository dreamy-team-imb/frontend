$(document).ready(function () {
  /* Handling languages */
  const englishBtn = document.querySelector("#english-btn");
  const shqipBtn = document.querySelector("#shqip-btn");
  const deleteBtn = document.querySelector(".deleteBtn");
  const remove = document.querySelector(".remove");

  firebaseApp.auth().onAuthStateChanged(function (user) {
    showLoader();
    if (user) {
      // User is signed in.
      showLoader();
      //currentUser = user;
      $(".logIn").remove();
      $(".certificateUser").remove();

      document.querySelector(".username").innerHTML = firebaseApp
        .auth()
        .currentUser.getIdTokenResult()
        .then((idTokenResult) => {
          // Confirm the user is an Admin.
          if (idTokenResult.claims.admin) {
          } else {
            $(".adminDir").remove();
            console.log("Not admin");
          }
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      $(".signOutButton").remove();
      $(".certificateAdmin").remove();
      $(".userAccount").remove();
    }
    if (!user) {
      remove.style.display = "none";
    }
  });

  window.addEventListener("hashchange", function () {
    const languageHash = location.hash;

    const englishElements = document.querySelectorAll(".english-element");
    const shqipElements = document.querySelectorAll(".shqip-element");

    if (languageHash === "#english") {
      englishElements.forEach((element) => {
        element.classList.remove("hide");
      });

      shqipElements.forEach((element) => {
        element.classList.add("hide");
      });
    } else if (languageHash === "#shqip") {
      shqipElements.forEach((element) => {
        element.classList.remove("hide");
      });
      englishElements.forEach((element) => {
        element.classList.add("hide");
      });
    }
  });

  englishBtn.addEventListener("click", function () {
    window.location.hash = "english";
    event.preventDefault();
  });

  shqipBtn.addEventListener("click", function () {
    window.location.hash = "shqip";
    event.preventDefault();
  });

  let currentUserAdmin;
  //Check for user login
  firebaseApp.auth().onAuthStateChanged(function (user) {
    if (user) {
      firebaseApp
        .auth()
        .currentUser.getIdTokenResult()
        .then((idTokenResult) => {
          // Confirm the user is an Admin.
          if (idTokenResult.claims.admin) {
            currentUserAdmin = true;
          }
        })
        .catch((error) => {
          console.log(error);
        });
    } else {
      listedDiv.setAttribute("style", "display:none !important");
      listedCert.setAttribute("style", "display:none !important");
    }
  });

  let listedDiv = document.querySelector(".listedDiv");
  let listedCert = document.querySelector("#listed");
  let certId = window.location.search.split("=")[1];
  let websitePath = window.location.origin + "/view.html?serial=";

  if (!certId || certId == undefined) {
    window.location = "/";
    return false;
  }
  // Get raw template
  const template = $("#cert-template").html();
  const template_instructor = $("#cert-template-instructor").html();
  const template_analyst = $("#cert-template-analyst").html();
  const template_flutter = $("#cert-template-flutter").html();
  const template_delta = $("#cert-template-delta").html();

  // compilet template - ready to use
  const compiled = Handlebars.compile(template);
  const compiled_instrictor = Handlebars.compile(template_instructor);
  const compiled_analyst = Handlebars.compile(template_analyst);
  const compiled_flutter = Handlebars.compile(template_flutter);
  const compiled_delta = Handlebars.compile(template_delta);
  // const compiled_flutter_test = Handlebars.compile(template_flutter_test);

  function getStudentName(defValue, ref) {
    ref.get().then((doc) => {
      $(`[data-studentid="${ref.id}"]`).text(doc.data().name);
    });

    return defValue;
  }

  getOneCertBySerial(certId)
    .then((data) => {
      if (!data) {
        alert("Nuk ka certifikate me kete id.");
        window.location = "/";
        return false;
      }

      if (data.unlisted == false) {
        listedCert.checked = false;
      } else if (data.unlisted == undefined || data.unlisted == null) {
        listedCert.checked = false;
      } else if (data.unlisted == true && currentUserAdmin) {
        listedCert.checked = true;
      } else if (data.unlisted == true) {
        listedCert.style.display = "none";
        listedCert.checked = true;
      } else {
        window.location = "/certificates.html";
      }

      //Variables
      let downloadPDFBtn = document.querySelector(".downloadPDFBtn");
      let printBtn = document.querySelector(".printBtn");
      let pdfName = "CA_" + data.serial + ".pdf";
      let id = "pdf";
      let html;
      let typeMapShqip;
      let typeMapEnglish;

      let courseName;

      const dt = new Date(data.date.toDate());

      const day =
        dt.getDate().toString().length > 1
          ? dt.getDate()
          : "0" + dt.getDate().toString();
      const month =
        (dt.getMonth() + 1).toString().length > 1
          ? dt.getMonth() + 1
          : "0" + (dt.getMonth() + 1).toString();

      let formated = `${day}/${month}/${dt.getFullYear()}`;

      if (data.type == "user") {
        typeMapShqip =
          "Përdorues i <span class='text-primary ml-1'>programit financiar në Cloud</span>";
        typeMapEnglish =
          "User of <span class='text-primary ml-1'>financial software in the Cloud</span>";
        courseName = "Përdorues i programit financiar në Cloud";

        html = compiled({
          ref: data.student.id,
          student: data.studentData.name,
          instructor: data.instructor,
          date: formated,
          id: data.serial,
          typeShqip: typeMapShqip,
          typeEnglish: typeMapEnglish,
        });
      } else if (data.type == "instructor") {
        typeMapShqip = "Instruktor";
        typeMapEnglish = "Instructor";
        courseName = "Instruktor i programit financiar në Cloud";

        html = compiled_instrictor({
          ref: data.student.id,
          student: data.studentData.name,
          instructor: data.instructor,
          date: formated,
          id: data.serial,
          typeShqip: typeMapShqip,
          typeEnglish: typeMapEnglish,
        });
      } else if (data.type == "data-analyst") {
        typeMapShqip = "Data Analyst në Cloud";
        typeMapEnglish = "Data Analyst in the Cloud";
        courseName = "Data Analyst në Cloud";

        html = compiled_analyst({
          ref: data.student.id,
          student: data.studentData.name,
          instructor: data.instructor,
          date: formated,
          id: data.serial,
          typeShqip: typeMapShqip,
          typeEnglish: typeMapEnglish,
        });
      } else if (data.type == "data-flutter") {
        typeMapShqip = "Data Analyst në Flutter";
        typeMapEnglish = "Data Analyst in the Flutter";
        courseName = "Data Analyst në Flutter";

        html = compiled_flutter({
          ref: data.student.id,
          student: data.studentData.name,
          instructor: data.instructor,
          date: formated,
          id: data.serial,
          typeShqip: typeMapShqip,
          typeEnglish: typeMapEnglish,
        });
      }

      // else if (data.type == "data-flutter-test") {

      //     typeMapShqip = "Data Analyst në Flutter Test"
      //     typeMapEnglish = "Data Analyst in the Flutter Test"
      //     courseName = "Data Analyst në Flutter Test"

      //     html = compiled_flutter_test({
      //         ref: data.student.id,
      //         student: data.studentData.name,
      //         instructor: data.instructor,
      //         date: formated,
      //         id: data.serial,
      //         typeShqip: typeMapShqip,
      //         typeEnglish: typeMapEnglish
      //     })
      // }
      else if (data.type == "data-delta") {
        typeMapShqip = "Data Analyst në Delta";
        typeMapEnglish = "Data Analyst in Delta";
        courseName = "Data Analyst në Delta";

        html = compiled_delta({
          ref: data.student.id,
          student: data.studentData.name,
          instructor: data.instructor,
          date: formated,
          id: data.serial,
          typeShqip: typeMapShqip,
          typeEnglish: typeMapEnglish,
        });
      }
      /* Email text */

      const certLinkEncoded = `https%3A%2F%2Fcertificate.imb.al%2Fview%3Fserial%3D${data.serial}`;
      const emailText =
        "Kjo është certifikata ime pas përfundimit të kursit " +
        `"${courseName}"` +
        "." +
        "%0D%0A" +
        "%0D%0A" +
        certLinkEncoded +
        "%0D%0A" +
        "%0D%0A";
      $("#email-share-button").attr({
        href: `mailto:?subject=Certifikatë - IMB&body=${emailText}`,
      });

      $(".cert-view").html(html);

      // Execute
      new QRCode(document.getElementById("qrcode"), {
        text: `${websitePath}${data.serial}`,
        width: 90,
        height: 90,
        colorDark: "#000000",
        colorLight: "#ffffff",
        correctLevel: QRCode.CorrectLevel.H,
      });

      const languageHash = window.location.hash;

      const englishElements = document.querySelectorAll(".english-element");
      const shqipElements = document.querySelectorAll(".shqip-element");

      if (languageHash === "#english") {
        englishElements.forEach((element) => {
          element.classList.remove("hide");
        });

        shqipElements.forEach((element) => {
          element.classList.add("hide");
        });
      } else if (languageHash === "#shqip") {
        shqipElements.forEach((element) => {
          element.classList.remove("hide");
        });
        englishElements.forEach((element) => {
          element.classList.add("hide");
        });
      }

      downloadPDFBtn.addEventListener("click", () => {
        generatePDF(pdfName, id);
      });

      printBtn.addEventListener("click", () => {
        window.print();
      });

      //////////////////////DELETE

      const urlParams = new URLSearchParams(window.location.search);
      const serialCode = urlParams.get("serial");
      console.log(serialCode);

      deleteBtn.addEventListener("click", () => {
        db.collection("certificates")
          .get()
          .then((snap) => {
            snap.forEach((doc) => {
              // console.log(doc.data().serial);

              if (serialCode == doc.data().serial) {
                db.collection("certificates")
                  .doc(doc.id)
                  .delete()
                  .then(function () {
                    console.log("Document successfully deleted!");
                    alert("Document successfully deleted!");
                    window.location = "/certificates.html";
                  })
                  .catch(function (error) {
                    console.error("Error removing document: ", error);
                  });
              }
            });
          });
      });

      //////////////////////////////////
      listedCert.addEventListener("change", () => {
        updateListed(data.id, {
          unlisted: listedCert.checked,
        });
      });

      $("#link-share-button").click(() => {
        $("#share-cert-modal").modal("hide");
        copyToClipboard(window.location.href);
      });
    })
    .catch((err) => {
      console.log(err);
    });
});
