
//Selectors
let userModalTrigger = document.querySelector('.userModalTrigger')
let addUserBtn = document.querySelector('.addUserBtn')
let userEmail = document.querySelector('.user-email')
let userPassword = document.querySelector('.user-password')
let userName = document.querySelector('.user-name')
let userRole = document.querySelector('.user-role')
let $userTable = $(".userTable");
let signOutButton = document.querySelector('.signOutButton')
let passwordModalBtn = document.querySelector('.passwordModalBtn')
let passwordModalBody = document.querySelector('.passwordModal-body')
let changePasswordBtn = document.querySelector('.changePasswordBtn')
let closeModal = document.querySelector('.closeModal')
let userForm = '#add-user'
let currentuser;



//Check if there's a user and if he has admin claims
firebaseApp.auth().onAuthStateChanged(function (user) {
    showLoader()
    if (user) {
        currentuser = user
        console.log(user)
        $('.logIn').remove()
        $('.certificateUser').remove()
        document.querySelector('.username').innerHTML = user.displayName + '<i class="pl-1 fa fa-user"></i>'

        document.querySelector('.passwordModal-body').innerHTML = 'Do të marrësh një email tek: ' + user.email + ' për të ndryshuar fjalëkalimin?'

        passwordModalBtn.addEventListener("click", () => {
            $('#passwordModal').modal('show')
        })

        changePasswordBtn.addEventListener("click", () => {
            firebaseApp.auth().sendPasswordResetEmail(
                user.email)
                .then((success) => {
                    console.log("Password reset email sent.")
                    $('#passwordModal').modal('hide')
                })
                .catch((error) => {
                    // Error occurred. Inspect error.code.
                    console.log(error)
                });
        })

        closeModal.addEventListener("click", () => {
            $('#passwordModal').modal('hide')
        })

        firebaseApp.auth().currentUser.getIdTokenResult()
            .then((idTokenResult) => {
                console.log(idTokenResult)

                // Confirm the user is an Admin.
                if (idTokenResult.claims.admin) {

                    $('#userUI').remove()

                    function renderTableData(data) {
                        $userTable.html('')
                        if (data.length) {

                            for (let i = 0; i < data.length; i++) {

                                let disabled = "";
                                let status = "";
                                let isAdmin = "un";

                                /* Admin can't disable himself or other admins */
                                if (data[i].role == "admin") {
                                    disabled = "disabled";
                                    isAdmin = "";
                                }

                                /* Check if the user is on or off and modify slider */
                                if (data[i].status == false) {
                                    status = "un";
                                }


                                /* Create user html element */
                                let user = `
                                <tr>
                                    <td style="width:25%">${data[i].username}</td>
                                    <td style="width:45%">${data[i].email}</td>
                                    <td style="width:15%">
                                        <label class="switch">
                                            <input class="togglerStatus" ${disabled} data-docid=${data[i].docID} type="checkbox" ${status}checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                    <td style="width:15%">
                                        <label class="switch">
                                            <input class="togglerAdmin" data-uid=${data[i].uid} data-docid=${data[i].docID} type="checkbox" ${isAdmin}checked>
                                            <span class="slider round"></span>
                                        </label>
                                    </td>
                                </tr>`

                                /* Add user to the table */
                                $userTable.append(user)

                            }


                            /* User selectors */
                            let toggleStatus = document.querySelectorAll('.togglerStatus')
                            let togglerAdmin = document.querySelectorAll('.togglerAdmin')
                            let userTextArr = []
                            let userToToggle

                            /* Events for enabling / disabling  */
                            toggleStatus.forEach(el => {
                                el.addEventListener("click", (e) => {

                                    let docID = e.target.getAttribute('data-docid')
                                    if (e.target.hasAttribute('checked')) {
                                        e.target.removeAttribute('checked')
                                        updateUser(docID, {
                                            status: false
                                        }).then((success) => {})
                                        .catch((error) => console.log(error))

                                    }
                                    else if (!e.target.hasAttribute('checked')) {
                                        $(e.target).attr("checked", "checked")
                                        updateUser(docID, {
                                            status: true
                                        }).then((success) => {})
                                        .catch((error) => console.log(error))
                                    }
                                })
                            })



                            /* Events for enabling / disabling  */
                            togglerAdmin.forEach(el => {
                                el.addEventListener("click", (e) => {

                                    let uid = e.target.getAttribute('data-uid')
                                    let docId = e.target.getAttribute('data-docid')

                                    if (e.target.hasAttribute('checked')) {
                                        $(e.target).attr("disabled", "disabled")
                                        makeAdmin(uid, "delete").then(res => {
                                            console.log(res)
                                            updateUser(docId, {
                                                role: "user"
                                            }).then(user => {
                                                console.log(user)
                                            })
                                                .catch(error => {
                                                    console.log(error)
                                                })
                                        })
                                            .catch(error => {
                                                console.error(error)
                                            })
                                    }
                                    else if (!e.target.hasAttribute('checked')) {
                                        $(e.target).attr("checked", "checked")
                                        makeAdmin(uid, "add").then(res => {
                                            console.log(res)
                                            updateUser(docId, {
                                                role: "admin"
                                            }).then(user => {
                                                
                                            })
                                                .catch(error => {
                                                    console.log(error)
                                                })
                                        })
                                            .catch(error => {
                                                console.error(error)
                                            })
                                    }
                                })
                            })

                        } else {
                            $userTable.append('Nuk u gjet asnje perdorues.')
                        }
                    }


                    //Opens user modal
                    userModalTrigger.addEventListener('click', () => {
                        $('#userModal').modal('show')
                    })


                    //Get's user data and triggers cloud function
                    addUserBtn.addEventListener('click', (e) => {
                        e.preventDefault()
                        if (validateForm(userForm)) {

                            console.log(userEmail.value, userPassword.value, userName.value, userRole.value)
                            if (userRole.value == 'admin') {
                                createUser(userName.value, userRole.value, userEmail.value, userPassword.value).then((success) => {
                                    console.log(success)
                                    $('#userModal').modal('hide')
                                    makeAdmin(success.uid).then((assigned) => {
                                        console.log(assigned)
                                    })
                                    window.location.reload()
                                }).catch((error) => {
                                    console.log(error)
                                    $('#userModal').modal('hide')
                                })
                            }
                            else if (userRole.value == 'user') {
                                createUser(userName.value, userRole.value, userEmail.value, userPassword.value).then((success) => {
                                    console.log(success)
                                    $('#userModal').modal('hide')
                                    window.location.reload()
                                }).catch((error) => {
                                    console.log(error)
                                    $('#userModal').modal('hide')
                                })
                            }
                        }

                    })

                    getUsers().then(data => {
                        renderTableData(data)
                        console.log(data)

                    }).catch(err => {
                        console.log(err)
                    })

                } else {

                    $('#adminUI').remove()
                    $('.adminDir').remove()
                    console.log('Not admin')
                }
            })
            .catch((error) => {
                console.log(error);
            });

    } else {
        window.location = '/'
        $('.signOutButton').remove()
        $('.certificateAdmin').remove()
        $('.userAccount').remove()
    }
});

//Log out user
signOutButton.addEventListener("click", () => {
    signOut().then((success) => {

        window.location.reload()
    }).catch((error) => {
        console.log(error)
    })
});





