// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries -->

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyBjNctKnrd9S91FNpguIUL2GCPEfdfG7ws",
  authDomain: "imb-digital-cert.firebaseapp.com",
  projectId: "imb-digital-cert",
  storageBucket: "imb-digital-cert.appspot.com",
  messagingSenderId: "997485242377",
  appId: "1:997485242377:web:708e60af83b82b6e15b0d8",
};

// Initialize Firebase
var firebaseApp = firebase.initializeApp(firebaseConfig);

var db = firebaseApp.firestore();
var certCollection = db.collection("certificates");
var studentCollection = db.collection("students");
var userCollection = db.collection("users");
var lastStudentCode = db.collection("settings").doc("lastStudentCode");
var lastCertCode = db.collection("settings").doc("lastCertCode");

//First chunk
var firstCertChunk = certCollection.orderBy("serial", "desc").limit(10);
