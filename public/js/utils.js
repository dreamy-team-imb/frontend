var currentUser;
//Sign Out
function signOut() {
  return new Promise((reject, resolve) => {
    firebaseApp
      .auth()
      .signOut()
      .then((m) => {
        resolve(m);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

//Form validation
function validateForm(form) {
  let formElement = document.querySelector(form);
  formElement.classList.add("was-validated");

  if (!formElement.checkValidity()) {
    document
      .querySelectorAll(form + " input.validate")
      .forEach(function (input) {
        if (!input.checkValidity()) {
          input.classList.add("invalid");
        }
      });
    return false;
  } else {
    formElement.classList.remove("was-validated");
    return true;
  }
}

//Loader -> show & hide
function showLoader() {
  $("#preloader")
    .css("visibility", "visible")
    .css("display", "block")
    .css("opacity", 1)
    .fadeIn();
}

function hideLoader() {
  $("#preloader").animate(
    {
      opacity: "0",
    },
    600,
    function () {
      setTimeout(function () {
        $("#preloader").css("display", "none").fadeOut();
      }, 300);
    }
  );
}

function clearForm(form) {
  return document.querySelector(form).reset();
}

//Add a certificate
async function addCert(data) {
  /* Add new Certificate to firesotre */
  certCollection
    .add(data)
    .then((doc) => {
      var option = {
        animation: true,
        delay: 4000,
      };

      showLoader();
      $("#emailModal").modal("hide");
      hideLoader();

      document.querySelector(".toastheadtext").innerHTML = "ID: " + doc.id;
      document.querySelector(".toast-body").innerHTML =
        "Certifikata u shtua me sukses!";
      $(".toast").toast(option);
      $(".toast").toast("show");
    })
    .catch((error) => {
      alert(error.message);
    });
}




//Add a student


async function addStudent(data) {
  try {
    // Check if the user with the same name or email already exists
    const querySnapshot = await studentCollection
      .where('name', '==', name)
      .get();
      console.log(querySnapshot);
    if (!querySnapshot.empty) {
      // User with the same name or email already exists
      alert('User already exists with the same name.');
      return;
    }

    // If the user does not exist, add them to the Firestore
    await studentCollection
    .add(data)
    .then((doc) => {
      var option = {
        animation: true,
        delay: 4000,
      };

      document.querySelector(".toastheadtext").innerHTML = "ID: " + doc.id;
      document.querySelector(".toast-body").innerHTML =
        "Marresi u shtua me sukses!";
      $(".toast").toast(option);
      $(".toast").toast("show");
    })
    .catch((error) => {
      console.log(error);
      alert(error.message);
    });

}
catch (error) {
  console.error('Error adding user: ', error);
}
}

//Add a student
async function addUser(name, email) {
  try {
    // Check if the user with the same name or email already exists
    console.log(email);
    const querySnapshot = await studentCollection
      .where('name', '==', name)
      .orWhere('email', '==', email)
      .get();
      console.log(email);

    if (!querySnapshot.empty) {
      // User with the same name or email already exists
      alert('User already exists with the same name or email.');
      return;
    }

    // If the user does not exist, add them to the Firestore
    await studentCollection
    .add(data)
    .then((doc) => {
      console.log("j");
      updateCodeStudent({
        code: inc(data.code),
      });
    })
    .catch((error) => {
      console.log(error);
      alert(error.message);
    });

    // User added successfully
    alert('User added successfully.');
  } catch (error) {
    console.error('Error adding user: ', error);
    alert('An error occurred while adding the user.');
  }

}

function updateCode(codeUpadete) {
  /* Updates student code */
  lastStudentCode
    .set(codeUpadete)
    .then((success) => {
      console.log(success);
    })
    .catch((error) => {
      console.log(error);
    });
}

function updateCodeStudent(codeUpadete) {
  /* Updates student code */
  lastStudentCode
    .set(codeUpadete)
    .then((success) => {
      console.log("success");
      window.location.reload();
    })
    .catch((error) => {
      console.log(error);
    });
}

function updateCertCode(codeUpadete) {
  /* Updates certificate code */
  lastCertCode
    .set(codeUpadete)
    .then((success) => {
      console.log(success);
    })
    .catch((error) => {
      console.log(error);
    });
}

function updateUser(docID, update) {
  /* Updates user status in Firestore and trigger cloud function */
  return new Promise((resolve, reject) => {
    userCollection
      .doc(docID)
      .update(update)
      .then((success) => {
        resolve(docID);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function updateListed(docID, unlistedUpdate) {
  /* Updates certificate's listed property in Firestore */
  certCollection
    .doc(docID)
    .update(unlistedUpdate)
    .then((success) => {
      console.log(success);
    })
    .catch((error) => {
      console.log(error);
    });
}

function getAllStudents() {
  return new Promise((resolve, reject) => {
    let studentList = [];
    studentCollection
      .get()
      .then((snapshot) => {
        if (snapshot.size) {
          snapshot.forEach((student) => {
            studentList.push({ id: student.id, ...student.data() });
          });
          resolve(studentList);
        }
      })
      .catch((err) => {
        reject(err);
      });
  });
}

function getUsers() {
  return new Promise((resolve, reject) => {
    let usersList = [];
    userCollection.onSnapshot(
      (snapshot) => {
        if (snapshot.size) {
          snapshot.forEach((user) => {
            usersList.push({ docID: user.id, ...user.data() });
          });
          resolve(usersList);
        }
      },
      (error) => {
        reject(error);
      }
    );
  });
}

//Get all certificates by name
function getCertByName(student) {
  return new Promise(async (resolve, reject) => {
    let certListByName = [];

    const toTitleCase = (phrase) => {
      return phrase
        .toLowerCase()
        .split(" ")
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(" ");
    };

    const parsedName = toTitleCase(student.studentName);

    try {
      const students = await studentCollection
        .where("name", "==", parsedName)
        .get();

      const studentPromises = students.docs.map(async (student) => {
        const studentDocRef = studentCollection.doc(student.id);
        const certificates = await certCollection
          .where("student", "==", studentDocRef)
          .get();

        const certPromises = certificates.docs.map((certificate) => {
          const certData = {
            ...certificate.data(),
            id: certificate.id,
            studentData: student.data(),
          };
          return certData;
        });

        return Promise.all(certPromises);
      });

      const [certs] = await Promise.all(studentPromises);
      resolve(certs);
    } catch (err) {
      console.log(err);
      reject("nodata");
    }
  });
}

//Get Student by name
function getStudentByName(name, uid) {
  return new Promise(async (resolve, reject) => {
    let certListByName = [];

    const toTitleCase = (phrase) => {
      return phrase
        .toLowerCase()
        .split(" ")
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(" ");
    };

    const parsedName = toTitleCase(name);

    try {
      const students = await studentCollection
        .where("name", "==", parsedName)
        .where("createdBy", "==", uid)
        .get();

      const studentData = students.docs.map((student) => {
        const certData = { id: student.id, ...student.data() };
        return certData;
      });

      resolve(studentData);
    } catch (err) {
      console.log(err);
      reject("nodata");
    }
  });
}

//Get Student by name
function getStudentByNameAdmin(name) {
  return new Promise(async (resolve, reject) => {
    let certListByName = [];

    const toTitleCase = (phrase) => {
      return phrase
        .toLowerCase()
        .split(" ")
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(" ");
    };

    const parsedName = toTitleCase(name);

    try {
      const students = await studentCollection
        .where("name", "==", parsedName)
        .get();

      const studentData = students.docs.map((student) => {
        const certData = { id: student.id, ...student.data() };
        return certData;
      });

      resolve(studentData);
    } catch (err) {
      console.log(err);
      reject("nodata");
    }
  });
}

//Get Student by name
function getStudentByEmail(email, uid) {
  return new Promise(async (resolve, reject) => {
    try {
      const students = await studentCollection
        .where("email", "==", email)
        .where("createdBy", "==", uid)
        .get();
        console.log(students);
        console.log(email);
      const studentData = students.docs.map((student) => {
        const certData = { id: student.id, ...student.data() };
        return certData;
      });

      resolve(studentData);
    } catch (err) {
      console.log(err);
      reject("nodata");
    }
  });
}

//Get Student by name
function getStudentByEmailAdmin(email) {
  return new Promise(async (resolve, reject) => {
    try {
      const students = await studentCollection
        .where("email", "==", email)
        .get();

      const studentData = students.docs.map((student) => {
        const certData = { id: student.id, ...student.data() };
        return certData;
      });

      resolve(studentData);
    } catch (err) {
      console.log(err);
      reject("nodata");
    }
  });
}

//Get all certificates
function getCert() {
  return new Promise(async (resolve, reject) => {
    let certList = [];

    let lastVisible;
    let firstVisible;

    try {
      /* Get both student and certificates collection data from firestore - async */
      var [studentsRef, firstCertChunkRef] = await Promise.all([
        studentCollection.get(),
        firstCertChunk.get(),
      ]);
    } catch (err) {
      console.log("Error getting documents: ", err);
      reject(error);
    }

    /* Check for snapshot sizes */
    if (studentsRef.size && firstCertChunkRef.size) {
      /* Student data and id */
      let studentData = studentsRef.docs.map((student) => {
        return { ...student.data(), id: student.id };
      });

      /* Certificate data with detailed student information */
      let detailedCert = firstCertChunkRef.docs.map((cert) => {
        /* Return the student with the same ID as the student reference in the certificate.data() */
        currentStudent = studentData.filter((student) => {
          return cert.data().student.id == student.id;
        });
        return {
          ...cert.data(),
          id: cert.id,
          studentData: currentStudent[0] || null,
        };
      });

      lastVisible = firstCertChunkRef.docs[firstCertChunkRef.docs.length - 1];

      detailedCert.forEach((cert) => {
        certList.push(cert);
      });

      resolve(certList);

      $(".next").on("click", async function () {
        document.querySelector(".next").style.pointerEvents = "none";
        certList = [];
        var nextChunk = certCollection
          .orderBy("serial", "desc")
          .startAfter(lastVisible)
          .limit(10);

        try {
          /* Get both student and certificates collection data from firestore - async */
          var nextChunkRef = await nextChunk.get();
        } catch (err) {
          console.log("Error getting documents: ", err);
          reject(error);
        }

        if (nextChunkRef.empty == true) {
          document.querySelector(".next").style.pointerEvents = "none";
        } else {
          /* Certificate data with detailed student information */
          let detailedCert = nextChunkRef.docs.map((cert) => {
            /* Return the student with the same ID as the student reference in the certificate.data() */
            currentStudent = studentData.filter((student) => {
              return cert.data().student.id == student.id;
            });
            return {
              ...cert.data(),
              id: cert.id,
              studentData: currentStudent[0] || null,
            };
          });

          lastVisible = nextChunkRef.docs[nextChunkRef.docs.length - 1];
          firstVisible = nextChunkRef.docs[0];

          detailedCert.forEach((cert) => {
            certList.push(cert);
          });

          renderTableData(certList);
          document.querySelector(".next").style.pointerEvents = "auto";
          document.querySelector(".previous").style.pointerEvents = "auto";
        }
      });

      $(".previous").on("click", async function () {
        document.querySelector(".previous").style.pointerEvents = "none";
        certList = [];

        if (firstVisible != undefined && lastVisible.exists == true) {
          var previousChunk = certCollection
            .orderBy("serial", "desc")
            .endBefore(firstVisible)
            .limitToLast(10);

          try {
            /* Get both student and certificates collection data from firestore - async */
            var previousChunkRef = await previousChunk.get();
          } catch (err) {
            console.log("Error getting documents: ", err);
            reject(error);
          }

          /* Certificate data with detailed student information */
          let detailedCert = previousChunkRef.docs.map((cert) => {
            /* Return the student with the same ID as the student reference in the certificate.data() */
            currentStudent = studentData.filter((student) => {
              return cert.data().student.id == student.id;
            });
            return {
              ...cert.data(),
              id: cert.id,
              studentData: currentStudent[0] || null,
            };
          });

          if (detailedCert.length != 0) {
            lastVisible =
              previousChunkRef.docs[previousChunkRef.docs.length - 1];
            firstVisible = previousChunkRef.docs[0];

            if (firstVisible != undefined || lastVisible) {
              document.querySelector(".next").style.pointerEvents = "auto";

              detailedCert.forEach((cert) => {
                certList.push(cert);
              });

              renderTableData(certList);
              document.querySelector(".previous").style.pointerEvents = "auto";
            }
          }
        }
      });
    } else {
      reject("nodata");
    }
  });
}

function editStudentById(id, student) {
  studentCollection
    .doc(id)
    .update(student)
    .then((success) => {
      console.log(success);
      window.location.reload();
    })
    .catch((error) => {
      console.log(error);
    });
}

//Get all users's students
function getMyStudents(uid) {
  return new Promise(async (resolve, reject) => {
    let students = [];

    var lastVisible;
    var firstVisible;

    try {
      /* Get both student data from db */
      var studentSnapshot = await studentCollection
        .orderBy("code", "desc")
        .where("createdBy", "==", uid)
        .limit(10)
        .get();
      // const certData = { ...certSnapshot.docs[0].data(), id: certSnapshot.docs[0].id }
    } catch (err) {
      console.log("Error getting documents: ", err);
      reject(err);
    }

    /* Check for snapshot size */
    if (studentSnapshot.size) {
      /* Student data and id */
      var studentData = studentSnapshot.docs.map((student) => {
        return { ...student.data(), id: student.id };
      });
      lastVisible = studentSnapshot.docs[studentSnapshot.docs.length - 1];

      resolve(studentData);

      $(".next").on("click", async function () {
        document.querySelector(".next").style.pointerEvents = "none";
        students = [];

        try {
          /* Get Next data chunk - async */
          var nextChunk = await studentCollection
            .orderBy("code", "desc")
            .where("createdBy", "==", uid)
            .limit(10)
            .startAfter(lastVisible)
            .get();
        } catch (err) {
          console.log("Error getting documents: ", err);
          reject(err);
        }

        if (nextChunk.empty == true) {
          document.querySelector(".next").style.pointerEvents = "none";
        } else {
          /* Certificate data with detailed student information */
          let studentData = nextChunk.docs.map((student) => {
            return { ...student.data(), id: student.id };
          });
          lastVisible = nextChunk.docs[nextChunk.docs.length - 1];
          firstVisible = nextChunk.docs[0];

          resolve(studentData);
          renderTableData(studentData);
          document.querySelector(".next").style.pointerEvents = "auto";
          document.querySelector(".previous").style.pointerEvents = "auto";
        }
      });

      $(".previous").on("click", async function () {
        document.querySelector(".previous").style.pointerEvents = "none";
        certList = [];

        if (firstVisible != undefined && lastVisible.exists == true) {
          try {
            var previousChunk = await studentCollection
              .orderBy("code", "desc")
              .where("createdBy", "==", uid)
              .endBefore(firstVisible)
              .limitToLast(10)
              .get();
          } catch (err) {
            console.log("Error getting documents: ", err);
            reject(err);
          }

          /* Certificate data with detailed student information */
          let studentData = previousChunk.docs.map((student) => {
            return { ...student.data(), id: student.id };
          });

          if (studentData.length != 0) {
            lastVisible = previousChunk.docs[previousChunk.docs.length - 1];
            firstVisible = previousChunk.docs[0];

            if (firstVisible != undefined || lastVisible) {
              document.querySelector(".next").style.pointerEvents = "auto";
              resolve(studentData);
              renderTableData(studentData);
              document.querySelector(".previous").style.pointerEvents = "auto";
            }
          }
        }
      });
    } else {
      reject("nodata");
    }
  });
}

//Get all users's students - Admin
function getAllStudentsAdmin() {
  return new Promise(async (resolve, reject) => {
    let students = [];

    var lastVisible;
    var firstVisible;

    try {
      /* Get both student data from db */
      var studentSnapshot = await studentCollection
        .orderBy("code", "desc")
        .limit(10)
        .get();
    } catch (err) {
      console.log("Error getting documents: ", err);
      reject(err);
    }

    /* Check for snapshot size */
    if (studentSnapshot.size) {
      /* Student data and id */
      var studentData = studentSnapshot.docs.map((student) => {
        return { ...student.data(), id: student.id };
      });
      lastVisible = studentSnapshot.docs[studentSnapshot.docs.length - 1];

      resolve(studentData);

      $(".next").on("click", async function () {
        document.querySelector(".next").style.pointerEvents = "none";
        students = [];

        try {
          /* Get Next data chunk - async */
          var nextChunk = await studentCollection
            .orderBy("code", "desc")
            .limit(10)
            .startAfter(lastVisible)
            .get();
        } catch (err) {
          console.log("Error getting documents: ", err);
          reject(err);
        }

        if (nextChunk.empty == true) {
          document.querySelector(".next").style.pointerEvents = "none";
        } else {
          /* Certificate data with detailed student information */
          let studentData = nextChunk.docs.map((student) => {
            return { ...student.data(), id: student.id };
          });
          lastVisible = nextChunk.docs[nextChunk.docs.length - 1];
          firstVisible = nextChunk.docs[0];

          resolve(studentData);
          renderTableData(studentData);
          document.querySelector(".next").style.pointerEvents = "auto";
          document.querySelector(".previous").style.pointerEvents = "auto";
        }
      });

      $(".previous").on("click", async function () {
        document.querySelector(".previous").style.pointerEvents = "none";
        certList = [];

        if (firstVisible != undefined && lastVisible.exists == true) {
          try {
            var previousChunk = await studentCollection
              .orderBy("code", "desc")
              .endBefore(firstVisible)
              .limitToLast(10)
              .get();
          } catch (err) {
            console.log("Error getting documents: ", err);
            reject(err);
          }

          /* Certificate data with detailed student information */
          let studentData = previousChunk.docs.map((student) => {
            return { ...student.data(), id: student.id };
          });

          if (studentData.length != 0) {
            lastVisible = previousChunk.docs[previousChunk.docs.length - 1];
            firstVisible = previousChunk.docs[0];

            if (firstVisible != undefined || lastVisible) {
              document.querySelector(".next").style.pointerEvents = "auto";
              resolve(studentData);
              renderTableData(studentData);
              document.querySelector(".previous").style.pointerEvents = "auto";
            }
          }
        }
      });
    } else {
      reject("nodata");
    }
  });
}

// function getCertBySerial(code) {
//     //Get certificate by serial
//     return new Promise(async (resolve, reject) => {
//         let certListById = [];

//         try {
//             /* Get both student and certificates collection data from firestore - async */
//             var [studentsRef, certsRef] = await Promise.all([studentCollection.get(), certCollection.get()]);

//         } catch (err) {
//             console.log("Error getting documents: ", err);
//             reject(error)
//         }

//         /* Check for snapshot sizes */
//         if (studentsRef.size && certsRef.size) {

//             /* Student data and id */
//             let studentData = studentsRef.docs.map(student => { return { ...student.data(), id: student.id } });

//             /* Certificate data with detailed student information */
//             let detailedCert = certsRef.docs.map(cert => {
//                 /* Return the student with the same ID as the student reference in the certificate.data() */
//                 currentStudent = studentData.filter(student => { return cert.data().student.id == student.id })
//                 return {
//                     ...cert.data(), id: cert.id, studentData: currentStudent[0] || null
//                 }
//             })

//             detailedCert.forEach(cert => {

//                 let documentSerial = cert.serial

//                 if (documentSerial.includes(code.serial)) {
//                     certListById.push(cert);
//                 }
//             })

//             resolve(certListById)
//         } else {
//             reject("nodata")
//         }
//     })
// }

function getAllStudentsCerts(studentId) {
  return new Promise(async (resolve, reject) => {
    try {
      /* Create students ref */
      const studentDocRef = studentCollection.doc(studentId);

      const certSnapshot = await certCollection
        .where("student", "==", studentDocRef)
        .get();

      if (!certSnapshot.empty) {
        const certData = certSnapshot.docs.map((cert) => {
          return { ...cert.data(), id: cert.id };
        });
        resolve(certData);
        console.log(certData);
      } else {
        resolve([]);
      }
    } catch (err) {
      console.log(err);
      reject("nodata");
    }
  });
}

function getOneCertBySerial(serial, isArray) {
  //Get certificate by serial
  return new Promise(async (resolve, reject) => {
    try {
      const certSnapshot = await certCollection
        .where("serial", "==", serial)
        .get();

      const certData = {
        ...certSnapshot.docs[0].data(),
        id: certSnapshot.docs[0].id,
      };
      const studentData = await studentCollection
        .doc(certData.student.id)
        .get();

      const certificate = { ...certData, studentData: studentData.data() };

      if (isArray) {
        resolve([certificate]);
      } else {
        resolve(certificate);
      }
    } catch (err) {
      console.log(err);
      reject("nodata");
    }
  });
}

function getStudenById(student) {
  //Get certificate by serial
  return new Promise((resolve, reject) => {
    studentCollection
      .doc(student)
      .get()
      .then((snapshot) => {
        if (snapshot.empty) return resolve([]);

        // s//napshot.forEach(doc => {
        // let documentId = doc.data().id

        // if (documentSerial.includes(code.serial)) {
        // queryResult.push({ id: doc.id, ...doc.data() })
        // }
        // })

        let a = snapshot.data();
        resolve(a);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

function getStudent(student) {
  return new Promise((resolve, reject) => {
    studentCollection
      .get()
      .then((snapshot) => {
        snapshot.forEach((doc) => {
          if (doc.exists) {
            let documentId = doc.data().name;

            if (documentId == student.studentName) {
              resolve({ id: doc.id, ...doc.data() });
            }
          } else {
            resolve([]);
          }
        });
      })
      .catch((err) => {
        reject(err);
      });
  });
}

//Code incrementor
function inc(str) {
  let string = str;
  let numberIndex = 0;

  for (let i = 0; i < string.length; i++) {
    if (string[i] !== "0") {
      numberIndex = i;
      break;
    }
  }

  const num = parseInt(string.slice(numberIndex, string.length)) + 1;

  switch (num.toString().length) {
    case 1:
      string = `000${num}`;
      break;
    case 2:
      string = `00${num}`;
      break;
    case 3:
      string = `0${num}`;
      break;
    case 4:
      string = `${num}`;
      break;
  }
  return string;
}

//Cert code incrementor
function inc2(str) {
  let string = str;
  let numberIndex = 0;

  for (let i = 0; i < string.length; i++) {
    if (string[i] !== "0") {
      numberIndex = i;
      break;
    }
  }

  const num = parseInt(string.slice(numberIndex, string.length)) + 1;

  switch (num.toString().length) {
    case 1:
      string = `10000${num}`;
      break;
    case 2:
      string = `1000${num}`;
      break;
    case 3:
      string = `100${num}`;
      break;
    case 4:
      string = `10${num}`;
      break;
    case 5:
      string = `1${num}`;
      break;
    case 6:
      string = `${num}`;
      break;
  }
  return string;
}

function generatePDF(pdfName, id) {
  showLoader();
  const element = document.getElementById(id);

  element.style.margin = "0";
  element.classList.remove("mb-5");

  const opt = {
    margin: 0,
    filename: pdfName,
    pagebreak: { mode: "avoid-all", avoid: ".pagebreak" },
    image: { type: "jpeg", quality: 1 },
    html2canvas: {
      scale: 4,
    },
    jsPDF: {
      unit: "cm",
      format: "a4",
      orientation: "l",
      compress: false,
    },
  };

  html2pdf()
    .set(opt)
    .from(element)
    .save()
    .then(() => {
      element.style.marginTop = "70px";
      element.classList.add("mb-5");
      hideLoader();
    })
    .catch((err) => {
      hideLoader();
    });
}

function copyToClipboard(text) {
  // Create a temporary input element
  const input = document.createElement("input");
  input.setAttribute("type", "text");
  input.setAttribute("value", text);
  document.body.appendChild(input);

  // Select the text in the input element
  input.select();
  input.setSelectionRange(0, 99999); // For mobile devices

  try {
    // Copy the selected text to the clipboard
    document.execCommand("copy");

    // Display success message and close modal
    const successMessage = "Link copied to clipboard!";
    alert(successMessage);
  } catch (err) {
    // Display error message
    const errorMessage = "Unable to copy to clipboard. Please try again.";
    alert(errorMessage);
  }

  // Remove the temporary input element
  document.body.removeChild(input);
}
