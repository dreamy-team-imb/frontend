//Check for user login
firebaseApp.auth().onAuthStateChanged(function (user) {
  showLoader();
  if (user) {
    // User is signed in.
    showLoader();
    window.location = "/";
    //currentUser = user;
    $(".logIn").remove();
    $(".certificateUser").remove();
    firebaseApp
      .auth()
      .currentUser.getIdTokenResult()
      .then((idTokenResult) => {
        // Confirm the user is an Admin.
        if (idTokenResult.claims.admin) {
        } else {
          $(".adminDir").remove();
          console.log("Not admin");
        }
      })
      .catch((error) => {
        console.log(error);
      });
  } else {
    $(".signOutButton").remove();
    $(".certificateAdmin").remove();
    $(".userAccount").remove();
  }
});

//Selectors
let email = document.querySelector(".email");
let password = document.querySelector(".password");
let loginButton = document.querySelector(".loginBtn");
let signOutButton = document.querySelector(".signOutButton");
let logIn = document.querySelector(".logIn");
let loginErrors = document.querySelector(".login-errors");

/* Change password modal */
let passwordModalBtn = document.querySelector(".passwordModalBtn");
let closeModal = document.querySelector(".closeModal");
let changePasswordBtn = document.querySelector(".changePasswordBtn");
let resetByMail = document.querySelector(".resetByMail");
let modalErrors = document.querySelector(".modal-errors");

loginButton.addEventListener("click", () => {
  // Sign in
  if (validateForm("#login-form")) {
    //  if (email.value != null && email.value != "" && password.value != null && password.value != "") {
    firebaseApp
      .auth()
      .signInWithEmailAndPassword(email.value, password.value)
      .then((success) => {
        console.log(success.user.uid);
        window.location = "/certificates.html";
      })
      .catch((err) => {
        if (err.code == "auth/wrong-password") {
          loginErrors.style.display = "block";
          loginErrors.innerHTML = "Fjalekalimi eshte i gabuar!";
        } else if (err.code == "auth/user-not-found") {
          loginErrors.style.display = "block";
          loginErrors.innerHTML = "Nuk ka perdorues me kete email!";
        } else if (err.code == "auth/too-many-requests") {
          loginErrors.style.display = "block";
          loginErrors.innerHTML =
            "Shume prova te gabuara per tu loguar! Provo perseri me vone ose ndrysho fjalekalimin.";
        } else {
          loginErrors.style.display = "block";
          loginErrors.innerHTML = "Diçka shkoi keq!";
        }
        console.log(err);
      });
    //  }
  }
});

passwordModalBtn.addEventListener("click", () => {
  $("#passwordModal").modal("show");
});

closeModal.addEventListener("click", () => {
  $("#passwordModal").modal("hide");
  resetByMail.style.border = "1px #ced4da solid";
  modalErrors.innerHTML = "";
  resetByMail.value = "";
});

resetByMail.addEventListener("change", (e) => {
  e.target.style.border = "1px #ced4da solid";
});

changePasswordBtn.addEventListener("click", () => {
  if (resetByMail.value) {
    firebaseApp
      .auth()
      .sendPasswordResetEmail(resetByMail.value)
      .then((success) => {
        console.log("Password reset email sent.");
        modalErrors.style.color = "green";
        modalErrors.innerHTML = "Email-i u dergua!";
        setTimeout(() => {
          $("#passwordModal").modal("hide");
        }, 1000);
      })
      .catch((error) => {
        // Error occurred. Inspect error.code.
        if (error.code == "auth/invalid-email") {
          modalErrors.innerHTML = "Email-i nuk eshte i sakte!";
        }
        console.log(error);
      });
  } else {
    resetByMail.style.border = "1px rgb(206, 9, 9) solid";
  }
});
